package es.jalonso.uned.pfg.deep.nlp.word2vec;

import java.util.Collection;

import org.canova.api.util.ClassPathResource;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.BasicLineIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.stopwords.StopWords;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.deeplearning4j.ui.UiServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Ejemplo sencillo para entender la representación de texto en Word2Vec usando
 * la librería java <code>DeepLearning4j</code>
 * 
 * Inicialmente se buscan las palabras más cercanas a 'quijote', pero debemos
 * obviar los determinantes, conjunciones, preposiciones.
 * Problema: Más cercanas a quijote = [le, el, por, hecho, en, del, se, venta, las, dos]
 * 
 * TODO Incluir la librería ixa-pipes para afinar en el descubrimiento de palabras
 * y solucionar el problema anterior.
 * 
 * @author jalonso244
 */
public class SimpleWord2VecRawText {

    private static Logger log = LoggerFactory.getLogger(SimpleWord2VecRawText.class);

    public static void main(String[] args) throws Exception {

        String filePath = new ClassPathResource("texto.txt").getFile().getAbsolutePath();

        log.info("Cargamos y convertimos en vectores las frases....");
        // Trim en cada una de las frases.
        SentenceIterator iter = new BasicLineIterator(filePath);
        // Elegimos las palabras (tokens entre espacios) de las frases
        TokenizerFactory t = new DefaultTokenizerFactory();
        t.setTokenPreProcessor(new CommonPreprocessor());

        log.info("Construcción del modelo....");
        Word2Vec vec = new Word2Vec.Builder()
                .minWordFrequency(5)
                //.stopWords(StopWords.getStopWords())
                //.sampling(1)
                .iterations(1)
                .layerSize(100)
                .seed(42)
                .windowSize(5)
                .iterate(iter)
                .tokenizerFactory(t)
                .build();

        log.info("Entrenando el modelo de Word2Vec....");
        vec.fit();

        log.info("Escribimos el resultado en un archivo de texto....");

        WordVectorSerializer.writeWordVectors(vec, "target/vectoresporpalabra.txt");
        
        log.info("Palabras más cercanas a Quijote:");
        Collection<String> lst = vec.wordsNearest("quijote", 10);
        System.out.println(lst);
        
        // Se levanta un servidor web con enpoints de word2vec para jugar con el corpus.
        UiServer server = UiServer.getInstance();
        System.out.println("Started on port " + server.getPort());        
    }
}
