package es.jalonso.uned.pfg.deep.nlp.doc2vec;

import java.util.List;

import org.canova.api.util.ClassPathResource;
import org.deeplearning4j.berkeley.Pair;
import org.deeplearning4j.models.embeddings.inmemory.InMemoryLookupTable;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.text.documentiterator.LabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelledDocument;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.jalonso.uned.pfg.deep.nlp.doc2vec.util.FileLabelAwareIterator;
import es.jalonso.uned.pfg.deep.nlp.doc2vec.util.LabelSeeker;
import es.jalonso.uned.pfg.deep.nlp.doc2vec.util.MeansBuilder;

/**
 * Ejemplo que  nos permitirá la clasificación de documentos a través de los llamados ParagraphVectors.
 * Los vectores de parrafos se usan de forma similar a Latent Dirichlet allocation (LDA).
 *
 * Partimos de ejemplos que están etiquetados y nos serviran para el entrenamiento. Por tanto, el objetivo
 * es clasificar los textos no etiquetados.
 *
 * @author jalonso244
 */
public class SimpleDoc2VecClassifier {

    private static final Logger log = LoggerFactory.getLogger(SimpleDoc2VecClassifier.class);

    public static void main(String[] args) throws Exception {
        ClassPathResource resource = new ClassPathResource("doc2vec/etiquetado");

        // Se construye un iterador para el conjunto de datos.
        LabelAwareIterator iterator = new FileLabelAwareIterator.Builder()
                .addSourceFolder(resource.getFile())
                .build();

        TokenizerFactory t = new DefaultTokenizerFactory();
        t.setTokenPreProcessor(new CommonPreprocessor());

        // Configuración del entrenamiento para los Doc2Vec
        ParagraphVectors paragraphVectors = new ParagraphVectors.Builder()
                .learningRate(0.025)
                //.stopWords(StopWords.getStopWords())
                .minLearningRate(0.001)
                .minWordFrequency(4)
                .batchSize(1000)
                .epochs(10)
                .negativeSample(10)
                .iterate(iterator)
                .trainWordVectors(true)
                .tokenizerFactory(t)
                .build();

        // Entrenamos el modelo.
        paragraphVectors.fit();
        
        WordVectorSerializer.writeWordVectors(paragraphVectors, "target/paragraphvectors.txt");
        
        // Cargamos los documentos no etiquetados para clasificarlos.
        ClassPathResource unlabeledResource = new ClassPathResource("doc2vec/noetiquetado");

        FileLabelAwareIterator unlabeledIterator = new FileLabelAwareIterator.Builder()
                .addSourceFolder(unlabeledResource.getFile())
                .build();

        MeansBuilder meansBuilder = new MeansBuilder((InMemoryLookupTable<VocabWord>) paragraphVectors.getLookupTable(), t);
        LabelSeeker seeker = new LabelSeeker(iterator.getLabelsSource().getLabels(), (InMemoryLookupTable<VocabWord>)  paragraphVectors.getLookupTable());

        while (unlabeledIterator.hasNextDocument()) {
            LabelledDocument document = unlabeledIterator.nextDocument();

            INDArray documentAsCentroid = meansBuilder.documentAsVector(document);
            List<Pair<String, Double>> scores = seeker.getScores(documentAsCentroid);

            // Mostramos el resultado para todas las etiquetas, el de mayor valor será la etiqueta elegida.
            log.info("Documento '" + document.getLabel() + "' aproximación a cada etiqueta: ");
            for (Pair<String, Double> score: scores) {
                log.info("        " + score.getFirst() + ": " + score.getSecond());
            }
        }
    }
}
