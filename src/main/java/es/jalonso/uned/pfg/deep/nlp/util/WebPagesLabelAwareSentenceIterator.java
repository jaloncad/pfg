package es.jalonso.uned.pfg.deep.nlp.util;

import java.io.BufferedReader;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.deeplearning4j.text.documentiterator.LabelsSource;
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor;
import org.deeplearning4j.text.sentenceiterator.labelaware.LabelAwareSentenceIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;
import com.kohlschutter.boilerpipe.extractors.KeepEverythingExtractor;

import lombok.NonNull;

/**
 * Nos permite iterar cada uno de los documentos del dataset
 * 
 * Al parsear el dataset original, archivos txt con metadatos leemos
 * <code>DATASET</code> el metadato del label al que pertenece.
 * <code>HTML</code> el contenido del la página web original.
 * 
 * Al leer el contenido HTML nos quedamos únicamente con las palabras
 * de más de dos letras y obviamos los números y otros caracteres.
 * 
 * @author jalonso244
 *
 */
public class WebPagesLabelAwareSentenceIterator implements LabelAwareSentenceIterator {
	
	private static final Logger log = LoggerFactory.getLogger(WebPagesLabelAwareSentenceIterator.class);

	private static final String HTML_LINE = "HTML";
	private static final String DATASET = "DATASET";
	private static final String TITLE_PATTERN = "<title>(.*?)</title>";
	protected List<File> files;
	protected AtomicInteger position = new AtomicInteger(0);
	protected LabelsSource labelsSource;
	protected List<String> currentLabels;

	/**
	 * Constructor protegido ya que usamos el patrón builder para instanciar la clase.
	 * 
	 * @param files - El directorio donde se encuentra el dataset
	 * @param source - Guardaremos la lista de labels de los documentos en nuestro caso de clasificación son 2.
	 */
	protected WebPagesLabelAwareSentenceIterator(@NonNull List<File> files, @NonNull LabelsSource source) {
		this.files = files;
		this.labelsSource = source;
		this.currentLabels = new ArrayList<String>();
	}

	/* (non-Javadoc)
	 * @see org.deeplearning4j.text.sentenceiterator.SentenceIterator#nextSentence()
	 */
	@Override
	public String nextSentence() {
		File fileToRead = files.get(position.getAndIncrement());

		try {
			BufferedReader reader = Files.newReader(fileToRead, Charset.defaultCharset());

			StringBuilder builder = new StringBuilder();
			String line;
			
			// Hay documentos que no tienen contenido y como única referencia nos quedamos con el title.
			String doctemp="notitle";
			Pattern p = Pattern.compile(TITLE_PATTERN, Pattern.DOTALL); 
			boolean html = false;
			boolean alreadyLabeled = false;
			currentLabels.clear();
			while ((line = reader.readLine()) != null) {
				
				// Lectura del contenido de la página original.
				if (html) {
					builder.append(line).append(" ");
		      		Matcher m = p.matcher(line);
		      		while (m.find()) {
		      			if (m.group(1).length() > 0)
		      				doctemp = m.group(1).trim();
		      		}
					continue;
				}

				// Buscamos la etiqueta en el metadato DATASET
				if (!alreadyLabeled) {
					if (line.startsWith(DATASET)) {
						alreadyLabeled = true;
						currentLabels.add(line.split("=")[1]);
						// TODO labelSource ???
					}
				}

				// A partir del metadato HTML empieza el contenido real.
				if (line.startsWith(HTML_LINE)) {
					html = true;
					String[] tokens = line.split("=");
					if (tokens.length == 2)
						builder.append(tokens[1]);
				}
			}

			// Eliminamos etiquetas html y nos quedamos con los textos más largos
			//  que continen frases o parrafos.
			String doc = KeepEverythingExtractor.INSTANCE.getText(builder.toString())
					.replaceAll("[^a-zA-Z]", " ")
		      		.replaceAll("\\b[\\w']{1,2}\\b", "")
		      		.replaceAll("\\s{2,}", " ")
		      		.toLowerCase();
			
			// Si está vacía la página o no encontramos datos nos fijamos en el title.
			if (doc.isEmpty())
				doc = doctemp;
			
			if (doc == null || doc.equals("")) {
				System.out.println("Warning: " + fileToRead.getName());
				doc = "NaN";
			}
			
			// Guardamos la label del documento.
			labelsSource.storeLabel(currentLabels.get(0));
			
			log.debug("count " + currentLabels.size() + " file " + fileToRead);
			for (int i = 0; i < currentLabels.size(); i++)
				log.debug("labels: " + currentLabels().get(i));
			return doc;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean hasNext() {
		return position.get() < files.size();
	}

	@Override
	public void reset() {
		position.set(0);

	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub

	}

	@Override
	public SentencePreProcessor getPreProcessor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPreProcessor(SentencePreProcessor preProcessor) {
		// TODO Auto-generated method stub

	}

	@Override
	public String currentLabel() {
		if (this.currentLabels.size() > 0)
			return this.currentLabels.get(0);
		return "";
	}

	@Override
	public List<String> currentLabels() {
		return this.currentLabels;
	}

	/**
	 * Clase Builder para instanciar el iterador.
	 * 
	 * @author jaloncad
	 *
	 */
	public static class Builder {
		protected List<File> foldersToScan = new ArrayList<>();

		public Builder() {

		}

		/**
		 * Directorio donde se encuentra el dataset.
		 * 
		 * @param folder
		 * @return
		 */
		public Builder addSourceFolder(@NonNull File folder) {
			foldersToScan.add(folder);
			return this;
		}

		public WebPagesLabelAwareSentenceIterator build() {

			List<File> fileList = new ArrayList<>();
			List<String> labels = new ArrayList<>();

			for (File file : foldersToScan) {
				if (!file.isDirectory())
					continue;

				File[] files = file.listFiles();
				if (files == null || files.length == 0)
					continue;

				for (File fileDoc : files) {
					if (!fileDoc.isDirectory() && fileDoc.getName().endsWith(".txt")) {
						fileList.add(fileDoc);
					}
				}
			}
			
			// Descolocamos la lista de archivos para que no estén en orden.
			Collections.shuffle(fileList);
			LabelsSource source = new LabelsSource(labels);
			WebPagesLabelAwareSentenceIterator iterator = new WebPagesLabelAwareSentenceIterator(fileList, source);

			return iterator;
		}
	}

	public LabelsSource getLabelsSource() {
		return labelsSource;
	}

	public void setLabelsSource(LabelsSource labelsSource) {
		this.labelsSource = labelsSource;
	}

}
