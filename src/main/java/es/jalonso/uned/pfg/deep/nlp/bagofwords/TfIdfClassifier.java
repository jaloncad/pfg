package es.jalonso.uned.pfg.deep.nlp.bagofwords;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.deeplearning4j.bagofwords.vectorizer.TextVectorizer;
import org.deeplearning4j.bagofwords.vectorizer.TfidfVectorizer;
import org.deeplearning4j.datasets.iterator.DataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.jalonso.uned.pfg.deep.nlp.doc2vec.util.StopWordsAndStemmingPreprocessor;
import es.jalonso.uned.pfg.deep.nlp.util.WebPagesLabelAwareSentenceIterator;
import es.jalonso.uned.pfg.deep.nlp.util.WebPagesVectorizerLabelAwareDataSetIterator;

/**
 * Ejemplo que nos permitirá la clasificación de documentos a través de los
 * llamados Bag of Words. TODO Los vectores de parrafos se usan de forma similar
 * a Latent Dirichlet allocation (LDA).
 *
 * Partimos de ejemplos que están etiquetados y nos serviran para el
 * entrenamiento. Por tanto, el objetivo es clasificar los textos no
 * etiquetados.
 *
 * @author jalonso244
 */
public class TfIdfClassifier {

	private static final Logger log = LoggerFactory.getLogger(TfIdfClassifier.class);

	private static String dspath = "";
	private static int dtlen = 10;
	private static Boolean swords = true;
	private static Boolean stemming = true;

	public static void main(String[] args) throws Exception {

		// Creamos el parseador de los argumentos.
		CommandLineParser parser = new DefaultParser();

		Options options = new Options();
		options.addOption("ds", "dspath", true, "El path absoluto del directorio donde se encuentra el dataset"
				+ " debe contener únicamente dos clases de documentos");
		options.addOption("dt", "dtlen", true,
				"Entero para decidir la parte del dataset que será usada para el trainingset (default 10)");
		options.addOption("sw", "swords", true,
				"true/false dependiendo si queremos filtrar las stopwords (default true)");
		options.addOption("st", "stemming", true,
				"true/false dependiendo si queremos identificar las palabras por su raiz (default true)");

		try {
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("ds"))
				dspath = line.getOptionValue("ds");
			else
				throw new ParseException("dspath es obligatorio");
			if (line.hasOption("dt"))
				dtlen = Integer.valueOf(line.getOptionValue("dt")).intValue();
			if (line.hasOption("sw"))
				swords = Boolean.valueOf(line.getOptionValue("sw"));
			if (line.hasOption("st"))
				stemming = Boolean.valueOf(line.getOptionValue("st"));

		} catch (ParseException exp) {
			HelpFormatter formatter = new HelpFormatter();
    		formatter.printHelp( "java -Dloglevel=INFO (default DEBUG) " + TfIdfClassifier.class.getCanonicalName() + " [OPTIONS]", options );
			System.exit(0);
		}

		log.info("Empezando la clasificación de documentos...");  
		
		// Hacemos referencia al path donde se encuentra el dataset
		File dsfile = new File(dspath);

		// Construimos el iterador que nos permitirá reccorrer cada uno de los documentos.
		WebPagesLabelAwareSentenceIterator iterator = new WebPagesLabelAwareSentenceIterator.Builder()
				.addSourceFolder(dsfile).build();

		// Realizamos el pre-proceso de los documentos en base a los argumentos introducidos.
		TokenizerFactory t = new DefaultTokenizerFactory();
		t.setTokenPreProcessor(
				new StopWordsAndStemmingPreprocessor.Builder().doStopWords(swords).doStemming(stemming).build());

		// Creamos las representaciones vectoriales de los documentos.
		TextVectorizer vectorizer = new TfidfVectorizer.Builder().setIterator(iterator).setMinWordFrequency(500)
				.setTokenizerFactory(t).build();
		vectorizer.fit();

		log.debug("vocab: " + vectorizer.getVocabCache().vocabWords().toString());

		iterator.reset();
		
		// Creamos el dataset iterator para su uso en la red neuronal profunda.
		DataSetIterator iidsi = new WebPagesVectorizerLabelAwareDataSetIterator(vectorizer, iterator);

		log.info("total examples: " + iidsi.totalExamples() + " *********************************");
		log.info("total columns: " + iidsi.inputColumns() + " ***********************************"); // +
																												// iidsi.next().get(0));
		int outputNum = 2;
		int iterations = 10;
		long seed = 123;
		int hidden = 20;

		log.info("Build model....");
		
		// Configuración de la red neuronal profunda
		MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder().seed(seed).iterations(iterations)
				.learningRate(0.005).regularization(true).l2(1e-4).list()
				.layer(0,
						new DenseLayer.Builder().nIn(iidsi.inputColumns()).nOut(hidden).activation("tanh")
								.weightInit(WeightInit.XAVIER).build())
				.layer(1,
						new DenseLayer.Builder().nIn(hidden).nOut(hidden).activation("tanh")
								.weightInit(WeightInit.XAVIER).build())
				.layer(2, new OutputLayer.Builder(LossFunction.MCXENT).weightInit(WeightInit.XAVIER)
						.activation("softmax").nIn(hidden).nOut(outputNum).build())
				.backprop(true).pretrain(false).build();

		// Inicializamos el modelo.
		MultiLayerNetwork model = new MultiLayerNetwork(conf);
		model.init();
		model.setListeners(new ScoreIterationListener(1000));

		// Partimos el dataset en los datos para aprendizaje y los de test.
		int splitTest = (int) iidsi.totalExamples() / dtlen;
		int splitTrain = iidsi.totalExamples() - splitTest;

		log.info("*********************************************** splittest " + splitTest + " splitTrain " + splitTrain);

		// Entrenamiento del modelo.
		int nTrainEpochs = 1;
		int count = 0;
		for (int i = 0; i < nTrainEpochs; i++) {
			for (int j = 0; j < splitTrain; j++) {
				if (iidsi.hasNext()) {
					DataSet next = iidsi.next();
					model.fit(next);
					count++;
				}
			}
		}

		System.out.println("train count ******************************************************* : " + count);

		Evaluation evaluation = new Evaluation(2);

		// Realizamos el test para comprobar la eficacia del modelo.
		for (int i = 0; i < splitTest; i++) {
			if (iidsi.hasNext()) {
				DataSet dsTest = iidsi.next();
				INDArray predicted = model.output(dsTest.getFeatureMatrix(), false);
				INDArray actual = dsTest.getLabels();
				log.debug("label array" + actual.toString() + " predicted " + predicted.toString());
				evaluation.eval(predicted, actual);
			}
		}

		// Volcamos en la consola el resultado
		System.out.println(evaluation.stats());

	}
}
