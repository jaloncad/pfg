/**
 * 
 */
package es.jalonso.uned.pfg.deep.nlp.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.deeplearning4j.bagofwords.vectorizer.TextVectorizer;
import org.deeplearning4j.datasets.iterator.DataSetIterator;
import org.deeplearning4j.text.sentenceiterator.labelaware.LabelAwareSentenceIterator;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;

/**
 * Data set Iterator para convertir cada uno de los documentos del iterador
 * de los archivos originales.
 * 
 * El TextVectorizer nos permite crear los vectores de los documentos junto
 * con su label correspondiente.
 * 
 * @author jalonso244
 *
 */
public class WebPagesVectorizerLabelAwareDataSetIterator implements DataSetIterator {
	
	private static final long serialVersionUID = 4327039497222071757L;
	
	protected final TextVectorizer vectorizer;
	protected final LabelAwareSentenceIterator iterator;
	protected final List<String> currentLabels;
	protected AtomicInteger position = new AtomicInteger(0);

	/**
	 * Constructor de la clase.
	 * 
	 * @param vectorizer
	 * @param iterator
	 */
	public WebPagesVectorizerLabelAwareDataSetIterator(TextVectorizer vectorizer, LabelAwareSentenceIterator iterator){
		this.vectorizer = vectorizer;
		this.iterator = iterator;
		this.currentLabels = new ArrayList<String>();
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		return iterator.hasNext(); 
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#next()
	 */
	@Override
	public DataSet next() {
		return next(position.getAndIncrement());
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException("Not implemented");
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#next(int)
	 */
	@Override
	public DataSet next(int num) {
		
		String doc = iterator.nextSentence();
		String label = iterator.currentLabel();
		
		return vectorizer.vectorize(doc, label);
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#totalExamples()
	 */
	@Override
	public int totalExamples() {
		return vectorizer.getVocabCache().totalNumberOfDocs();
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#inputColumns()
	 */
	@Override
	public int inputColumns() {
		return this.vectorizer.getVocabCache().numWords();
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#totalOutcomes()
	 */
	@Override
	public int totalOutcomes() {
		return 1;
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#reset()
	 */
	@Override
	public void reset() {
		position.set(0);
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#batch()
	 */
	@Override
	public int batch() {
		throw new UnsupportedOperationException("Not implemented");
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#cursor()
	 */
	@Override
	public int cursor() {
		return position.get();
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#numExamples()
	 */
	@Override
	public int numExamples() {
		return totalExamples();
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#setPreProcessor(org.nd4j.linalg.dataset.api.DataSetPreProcessor)
	 */
	@Override
	public void setPreProcessor(DataSetPreProcessor preProcessor) {
		throw new UnsupportedOperationException("Not implemented");
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#getLabels()
	 */
	@Override
	public List<String> getLabels() {
		return currentLabels;
	}

}
