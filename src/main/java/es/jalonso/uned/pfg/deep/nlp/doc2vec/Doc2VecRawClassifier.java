package es.jalonso.uned.pfg.deep.nlp.doc2vec;

import java.io.File;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.jalonso.uned.pfg.deep.nlp.doc2vec.util.StopWordsAndStemmingPreprocessor;
import es.jalonso.uned.pfg.deep.nlp.util.WebPagesLabelAwareSentenceIterator;

/**
 * Clasificación de documentos usando el modelo Doc2Vec (ParagraphVectors).
 * 
 * Se trata de una clasificación semi-supervisada, ya que en primer lugar
 * vectorizamos las palabras y parrafos, y a countinuación comparamos la similitud
 * de las 2 labels a cada uno de los textos del training set, basándonos en el valor medio.
 * 
 * @author jalonso244
 */
public class Doc2VecRawClassifier {

    private static final Logger log = LoggerFactory.getLogger(Doc2VecRawClassifier.class);
    private static String dspath = "";
    private static String dtpath = "";
    private static Boolean swords = true;
    private static Boolean stemming = true;

    public static void main(String[] args) throws Exception {
    	
    	// Creamos el parseador de los argumentos.
    	CommandLineParser parser = new DefaultParser();

    	Options options = new Options();
    	options.addOption( "ds", "dspath", true, "El path absoluto del directorio donde se encuentra el trainingSet"
    													+ " debe contener únicamente dos clases de documentos" );
    	options.addOption( "dtp", "dtpath", true, "El path absoluto del directorio donde se encuentra el testSet"
														+ " debe contener únicamente dos clases de documentos" );
    	options.addOption( "sw", "swords", true, "true/false dependiendo si queremos filtrar las stopwords (default true)");
    	options.addOption( "st", "stemming", true, "true/false dependiendo si queremos identificar las palabras por su raiz (default true)");
    	
    	try {
    		CommandLine line = parser.parse( options, args );
    		
    		if(line.hasOption("ds")) dspath = line.getOptionValue("ds");
    		else throw new ParseException("dspath es obligatorio");
    		if(line.hasOption("dtp")) dtpath = line.getOptionValue("dtp");
    		else throw new ParseException("dtpath es obligatorio");
    		if(line.hasOption("sw")) swords = Boolean.valueOf(line.getOptionValue("sw"));
    		if(line.hasOption("st")) stemming = Boolean.valueOf(line.getOptionValue("st"));
    	    
    	}
    	catch( ParseException exp ) {
    		log.info(exp.getMessage());
    		HelpFormatter formatter = new HelpFormatter();
    		formatter.printHelp( "java -Dloglevel=INFO (default DEBUG) " + Doc2VecRawClassifier.class.getCanonicalName() + " [OPTIONS]", options );
    		System.exit(0);
    	}
    	
    	log.info("Empezando la clasificación de documentos...");  
    	
    	// Hacemos referencia al path donde se encuentra el dataset
        File dsfile = new File(dspath);

        // Construimos el iterador que nos permitirá reccorrer cada uno de los documentos.
        WebPagesLabelAwareSentenceIterator iterator = new WebPagesLabelAwareSentenceIterator.Builder()
        		.addSourceFolder(dsfile)
        		.build();
        
        // Realizamos el pre-proceso de los documentos en base a los argumentos introducidos.
        TokenizerFactory t = new DefaultTokenizerFactory();
        t.setTokenPreProcessor(new StopWordsAndStemmingPreprocessor.Builder()
        		.doStopWords(swords)
        		.doStemming(stemming)
        		.build()
        		);
        
        // Configuración del entrenamiento para los Doc2Vec
        ParagraphVectors paragraphVectors = new ParagraphVectors.Builder()
                .learningRate(0.025)
                .useAdaGrad(true)
                .layerSize(200)
                .minLearningRate(0.001)
                .minWordFrequency(500)
                .batchSize(1000)
                .epochs(1)
                .iterate(iterator)
                .trainWordVectors(true)
                .tokenizerFactory(t)
                .build();

        // Entrenamos el modelo.
        paragraphVectors.fit();
        
        
        // Hacemos referencia al path donde se encuentra el testset
        File dtfile = new File(dtpath);

        // Construimos el iterador que nos permitirá reccorrer cada uno de los documentos del testSet.
        WebPagesLabelAwareSentenceIterator iteratorTest = new WebPagesLabelAwareSentenceIterator.Builder()
        		.addSourceFolder(dtfile)
        		.build();
        
        List<String> labels = iterator.getLabelsSource().getLabels();
        Evaluation evaluation = new Evaluation(labels);

        while(iteratorTest.hasNext()){
        	
        	String sentence = iteratorTest.nextSentence();
        	int idx = labels.indexOf(iteratorTest.currentLabel());
        	
        	// Calculamos la similitud de cada una de las labels posibles con el texto.
        	//  se usa la distancia del coseno sobre la media del vector del documento.
            double sim1 = paragraphVectors.similarityToLabel(sentence, labels.get(idx));
            double sim2 = paragraphVectors.similarityToLabel(sentence, labels.get(Math.abs(idx - 1)));
            
            log.debug("sim: " + sim1 + " sim2: " + sim2);
            
            evaluation.eval((sim1>sim2)?idx:Math.abs(idx - 1), idx);
        }

        System.out.println(evaluation.stats());

        System.out.println("----- Example complete -----");
    }
}
