package es.jalonso.uned.pfg.deep.nlp.doc2vec;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.deeplearning4j.datasets.iterator.DataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.jalonso.uned.pfg.deep.nlp.doc2vec.util.StopWordsAndStemmingPreprocessor;
import es.jalonso.uned.pfg.deep.nlp.util.WebPagesDoc2VecLabelAwareDataSetIterator;
import es.jalonso.uned.pfg.deep.nlp.util.WebPagesLabelAwareSentenceIterator;

/**
 * Ejemplo que  nos permitirá la clasificación de documentos a través de los llamados ParagraphVectors.
 * Los vectores de parrafos se usan de forma similar a Latent Dirichlet allocation (LDA).
 *
 * Partimos de ejemplos que están etiquetados y nos serviran para el entrenamiento. Por tanto, el objetivo
 * es clasificar los textos no etiquetados.
 *
 * @author jalonso244
 */
public class Doc2VecClassifier {

    private static final Logger log = LoggerFactory.getLogger(Doc2VecClassifier.class);
    
    private static String dspath = "";
    private static int dtlen = 10;
    private static Boolean swords = true;
    private static Boolean stemming = true;

    public static void main(String[] args) throws Exception {
    	
    	// Creamos el parseador de los argumentos.
    	CommandLineParser parser = new DefaultParser();

    	Options options = new Options();
    	options.addOption( "ds", "dspath", true, "El path absoluto del directorio donde se encuentra el trainingSet"
    													+ " debe contener únicamente dos clases de documentos" );
    	options.addOption("dt", "dtlen", true, "Entero para decidir la parte del dataset que será usada para el trainingset (default 10)");
    	options.addOption( "sw", "swords", true, "true/false dependiendo si queremos filtrar las stopwords (default true)");
    	options.addOption( "st", "stemming", true, "true/false dependiendo si queremos identificar las palabras por su raiz (default true)");
    	
    	try {
    		CommandLine line = parser.parse( options, args );
    		
    		if(line.hasOption("ds")) dspath = line.getOptionValue("ds");
    		else throw new ParseException("dspath es obligatorio");
    		if(line.hasOption("dt")) dtlen = Integer.valueOf(line.getOptionValue("dt")).intValue();
    		else throw new ParseException("dtpath es obligatorio");
    		if(line.hasOption("sw")) swords = Boolean.valueOf(line.getOptionValue("sw"));
    		if(line.hasOption("st")) stemming = Boolean.valueOf(line.getOptionValue("st"));
    	    
    	}
    	catch( ParseException exp ) {
    		log.info(exp.getMessage());
    		HelpFormatter formatter = new HelpFormatter();
    		formatter.printHelp( "java -Dloglevel=INFO (default DEBUG) " + Doc2VecRawClassifier.class.getCanonicalName() + " [OPTIONS]", options );
    		System.exit(0);
    	}
    	
    	log.info("Empezando la clasificación de documentos...");  
    	
    	// Hacemos referencia al path donde se encuentra el dataset
        File dsfile = new File(dspath);

        WebPagesLabelAwareSentenceIterator iterator = new WebPagesLabelAwareSentenceIterator.Builder()
        		.addSourceFolder(dsfile)
        		.build();
        
        TokenizerFactory t = new DefaultTokenizerFactory();
        t.setTokenPreProcessor(new StopWordsAndStemmingPreprocessor.Builder()
        		.doStopWords(swords)
        		.doStemming(stemming)
        		.build()
        		);
        
        // SplitTestAndTrain 

        // Configuración del entrenamiento para los Doc2Vec
        ParagraphVectors paragraphVectors = new ParagraphVectors.Builder()
                .learningRate(0.025)
                .useAdaGrad(true)
                .layerSize(200)
                .minLearningRate(0.001)
                .minWordFrequency(500)
                .batchSize(1000)
                .epochs(1)
                .iterate(iterator)
                .trainWordVectors(false)
                .tokenizerFactory(t)
                .build();

        // Entrenamos el modelo.
        paragraphVectors.fit();
        
        int vectorSize = paragraphVectors.getLayerSize();   //Size of the word vectors. 300 in the Google News model
        
        log.debug("vectorsize: " + vectorSize);
        
        int nEpochs = 1;        //Number of epochs (full passes of training data) to train on

        //Set up network configuration
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1)
                .updater(Updater.RMSPROP)
                .regularization(true).l2(1e-5)
                .weightInit(WeightInit.XAVIER)
                .gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue).gradientNormalizationThreshold(1.0)
                .learningRate(0.0018)
                .list()
                .layer(0, new GravesLSTM.Builder().nIn(vectorSize).nOut(200)
                        .activation("softsign").build())
                .layer(1, new GravesLSTM.Builder().nIn(200).nOut(100)
                        .activation("softsign").build())
                .layer(2, new RnnOutputLayer.Builder(LossFunction.MCXENT).activation("softmax")
                        .nIn(100).nOut(2).build())
                .pretrain(false).backprop(true).build();

        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();
        net.setListeners(new ScoreIterationListener(1));
        
        iterator.reset();
        DataSetIterator dataSetIter = //new AsyncDataSetIterator(
        		new WebPagesDoc2VecLabelAwareDataSetIterator(paragraphVectors, iterator);
        
        int splitTest = (int)dataSetIter.totalExamples()/dtlen;
        int splitTrain = dataSetIter.totalExamples() - splitTest;
        
        log.info("Starting training");
        int count=0;
        for( int i=0; i<nEpochs; i++ ){
        	for(int j=0;j < splitTrain; j++){
        		if(dataSetIter.hasNext()){
        			net.fit(dataSetIter.next());
        			count++;
        		}
        	}
        	log.debug("Count: " + count);
            log.debug("Epoch " + i + " complete. Starting evaluation:");

            Evaluation evaluation = new Evaluation();
            for(int z=0;z < splitTest; z++){
            	while(dataSetIter.hasNext()){
                    DataSet ds = dataSetIter.next();
                    INDArray features = ds.getFeatureMatrix();
                    INDArray labels = ds.getLabels();
                    INDArray predicted = net.output(features,false);

                    evaluation.evalTimeSeries(labels,predicted);
                }
            }
            
            dataSetIter.reset();

            System.out.println(evaluation.stats());
        }


        System.out.println("----- Example complete -----");
    }
}
