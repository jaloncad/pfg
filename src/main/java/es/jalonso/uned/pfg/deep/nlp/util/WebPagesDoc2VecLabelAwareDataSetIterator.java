/**
 * 
 */
package es.jalonso.uned.pfg.deep.nlp.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.deeplearning4j.datasets.iterator.DataSetIterator;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.text.documentiterator.LabelledDocument;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.util.FeatureUtil;

/**
 * Una de las pruebas realizadas que no ha sido exitosa.
 * 
 * @author jalonso244
 * 
 */
public class WebPagesDoc2VecLabelAwareDataSetIterator implements DataSetIterator {
	
	private static final long serialVersionUID = 4327039497222071757L;
	
//	protected final InvertedIndex<VocabWord> invertedIndex;
//	protected InvertedIndex<VocabWord>;
	protected final ParagraphVectors paragraphVectors;
	protected final WebPagesLabelAwareSentenceIterator iterator;
	protected final List<String> currentLabels;
	
	private final int vectorSize;
	protected AtomicInteger position = new AtomicInteger(0);

	public WebPagesDoc2VecLabelAwareDataSetIterator(ParagraphVectors paragraphVectors, WebPagesLabelAwareSentenceIterator iterator){
		this.paragraphVectors = paragraphVectors;
		this.vectorSize = paragraphVectors.lookupTable().layerSize();
		this.iterator = iterator;
		this.currentLabels = new ArrayList<String>();
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		return iterator.hasNext(); 
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#next()
	 */
	@Override
	public DataSet next() {
		return next(position.getAndIncrement());
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException("Not implemented");
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#next(int)
	 */
	@Override
	public DataSet next(int num) {
		
		String doc = iterator.nextSentence();
		String label = iterator.currentLabel();
		
		LabelledDocument ld = new LabelledDocument();
		ld.setContent(doc);
		ld.setLabel(label);
		
		// TODO next infervector returns null
		INDArray pvector = paragraphVectors.inferVector(ld);
		INDArray labelMatrix = FeatureUtil.toOutcomeVector(iterator.labelsSource.indexOf(label), iterator.labelsSource.size());

        return new DataSet(pvector, labelMatrix);
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#totalExamples()
	 */
	@Override
	public int totalExamples() {
		return this.paragraphVectors.getVocab().totalNumberOfDocs();
		//return this.inputFeatures;
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#inputColumns()
	 */
	@Override
	public int inputColumns() {
		return this.vectorSize;
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#totalOutcomes()
	 */
	@Override
	public int totalOutcomes() {
		return 1;
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#reset()
	 */
	@Override
	public void reset() {
		position.set(0);
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#batch()
	 */
	@Override
	public int batch() {
		throw new UnsupportedOperationException("Not implemented");
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#cursor()
	 */
	@Override
	public int cursor() {
		return position.get();
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#numExamples()
	 */
	@Override
	public int numExamples() {
		return totalExamples();
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#setPreProcessor(org.nd4j.linalg.dataset.api.DataSetPreProcessor)
	 */
	@Override
	public void setPreProcessor(DataSetPreProcessor preProcessor) {
		throw new UnsupportedOperationException("Not implemented");
	}

	/* (non-Javadoc)
	 * @see org.nd4j.linalg.dataset.api.iterator.DataSetIterator#getLabels()
	 */
	@Override
	public List<String> getLabels() {
		return currentLabels;
	}

}
