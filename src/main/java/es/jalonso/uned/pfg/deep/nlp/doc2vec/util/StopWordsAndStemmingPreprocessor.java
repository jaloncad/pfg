package es.jalonso.uned.pfg.deep.nlp.doc2vec.util;

import org.deeplearning4j.text.stopwords.StopWords;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;

import opennlp.tools.stemmer.PorterStemmer;

/**
 * Preproceso de palabras.
 * 
 * Por un lado se puede realizar el filtrado de stopwords
 * y por otro la lematización de las palabras para tener
 * una única por palabras de la misma familia.
 * 
 * @author jalonso244
 *
 */
public class StopWordsAndStemmingPreprocessor extends CommonPreprocessor {
	
	protected boolean stopWords;
	protected boolean stemming;

	protected StopWordsAndStemmingPreprocessor(){}
	
	@Override
	public String preProcess(String token) {
		
		token = super.preProcess(token);
		
		if (stopWords) {
			if (StopWords.getStopWords().contains(token)) token="";
		}
		
		if (!token.equals("") && stemming) {
			PorterStemmer ps = new PorterStemmer();
			token = ps.stem(token);
		}
		
		return token;
	}

	public static class Builder {
        
		protected boolean stopWords = false;
		protected boolean stemming = false;

        public Builder() {

        }

        public Builder doStopWords(boolean bool) {
            this.stopWords = bool;
            return this;
        }
        
        public Builder doStemming(boolean bool){
        	this.stemming = bool;
        	return this;
        }

        public StopWordsAndStemmingPreprocessor build() {

        	StopWordsAndStemmingPreprocessor pp = new StopWordsAndStemmingPreprocessor();
        	pp.stopWords = this.stopWords;
        	pp.stemming = this.stemming;

            return pp;
        }
	}
	
}
